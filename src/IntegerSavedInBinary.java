import javax.sound.midi.Soundbank;
import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.Scanner;

public class IntegerSavedInBinary {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter an integer: ");
        int number = sc.nextInt();

        int[] bitTable = new int[32];
        int index = 0;

        int[] zu1Table = new int[32];
        int indexZU1 = 0;

        int[] zu2Table = new int[32];
        int indexZU2 = 0;

        boolean isNegative = false;

        char sign;

        if (number >= 0) {
            isNegative = false;
        } else if (number < 0) {
            isNegative = true;
            number = -number;
        }

        if (isNegative == true) {
            sign = '-';
        } else {
            sign = '+';
        }

        int divideByTwo = number / 2;

        int bit = number % 2;

        bitTable[index] = bit;
        index++;


        while (divideByTwo != 0) {
            bit = divideByTwo % 2;
            bitTable[index] = bit;
            index++;
            divideByTwo = divideByTwo / 2;
        }

        if (isNegative == true) {
            bitTable[index] = 1;
        } else if (isNegative == false) {
            bitTable[index] = 0;
        }

        index++;

        System.out.println();

        System.out.println("Integer: " + sign + number + " saved in binary ZM: ");


        for (int i = 0; i < index; i++) {
            System.out.print(bitTable[index - i - 1]);
        }

        System.out.println();

        if (isNegative == false) {
            for (int i = 0; i < index; i++) {
                int value = bitTable[index - i - 1];
                zu1Table[indexZU1] = value;
                zu2Table[indexZU2] = value;
                indexZU1++;
                indexZU2++;
            }
        }

        System.out.println("Integer: " + sign + number + " saved in binary ZU1: ");

        if (isNegative == false) {
            for (int i = 0; i < indexZU1; i++) {
                System.out.print(zu1Table[i]);
            }
        }

        indexZU1 = 0;

        if (isNegative == true) {
            for (int i = 0; i < index; i++) {
                int value = bitTable[index - i - 1];
                zu1Table[indexZU1] = value;
                if (indexZU1 == 0) {
                    zu1Table[indexZU1] = value;
                } else if (indexZU1 != 0 && value == 1) {
                    zu1Table[indexZU1] = 0;
                } else if (indexZU1 != 0 && value == 0) {
                    zu1Table[indexZU1] = 1;
                }
                indexZU1++;
            }
        }

        if (isNegative == true) {
            for (int i = 0; i < indexZU1; i++) {
                System.out.print(zu1Table[i]);
            }
        }

        System.out.println();

        System.out.println("Integer: " + sign + number + " saved in binary ZU2: ");

        if (isNegative == false) {
            for (int i = 0; i < indexZU2; i++) {
                System.out.print(zu2Table[i]);
            }
        }

        indexZU2 = 0;

        int additionResult = 0;
        boolean isTheRest = true;

        if (isNegative == true) {
            for (int i = 0; i < indexZU1; i++) {
                if (zu1Table[indexZU1 - i - 1] == 0 && isTheRest == true) {
                    additionResult = 1;
                    zu2Table[i] = additionResult;
                    isTheRest = false;
                } else if (zu1Table[indexZU1 - i - 1] == 1 && isTheRest == false) {
                    additionResult = 1;
                    zu2Table[i] = additionResult;
                    isTheRest = false;
                } else if (zu1Table[indexZU1 - i - 1] == 1 && isTheRest == true) {
                    additionResult = 0;
                    zu2Table[i] = additionResult;
                    isTheRest = true;
                } else if (zu1Table[indexZU1 - i - 1] == 0 && isTheRest == false) {
                    additionResult = 0;
                    zu2Table[i] = additionResult;
                    isTheRest = false;
                }

            }

            if (isNegative == true) {
                for (int i = 0; i < indexZU1; i++) {
                    System.out.print(zu2Table[indexZU1 - i - 1]);
                }
            }

        }
    }
}
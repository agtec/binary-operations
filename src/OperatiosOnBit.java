public class OperatiosOnBit {

    public static void main(String[] args) {

        // and
        System.out.println("0b0101 & 0b1100: " + Integer.toBinaryString(0b0101 & 0b1100));

        // or
        System.out.println("0b1010 | 0b1100: " + Integer.toBinaryString(0b1010 | 0b1100));

        // xor
        System.out.println("0b1010 ^ 0b1100: " + Integer.toBinaryString(0b1010 ^ 0b1100));

        // shifting bits
        System.out.println("0b0001010>>1: " + Integer.toBinaryString(0b0001010>>1));
        System.out.println("0b0001010<<1: "  + Integer.toBinaryString(0b0001010<<1));


        // masking
        System.out.println(Integer.toBinaryString(0b1010 & 0b0010));

        if ((0b1010 & 0b0010 )!= 0) {
            System.out.println("bit 1 is set");
        }

        int b = 0b000000000000000000000000000000000000000000010101;
        b = b | 0b1000;

        System.out.println(Integer.toBinaryString(b));

    }
}
